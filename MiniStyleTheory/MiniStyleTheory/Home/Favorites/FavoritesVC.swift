//
//  FavoritesVC.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 25/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit

class FavoritesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    private var favorites = [Product.Data]()
    
    private let productRealm = ProductRealmDatabase()
    
    private var width = 200.0
    private var height = 400.0
    private var margin = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        toolbar.barTintColor = UIColor.white
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingData()
    }
    
    private func setupCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ProductViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductViewCell")
    }
    
    private func loadingData(){
        favorites = productRealm.getProduct()
        for product in favorites {
            product.loved = true
        }
        self.collectionView.reloadData()
    }
    
    // MARK: COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favorites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductViewCell", for: indexPath) as! ProductViewCell
        cell.setData(data: self.favorites[indexPath.row])
        cell.actionBlock = {
            if let productName = self.favorites[indexPath.row].name {
                self.productRealm.deleteRealmObject(name: productName)
            }
            self.loadingData()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = calculateWidth()
        let height = CGFloat(self.height)
        return CGSize(width: width, height: height)
    }
    
    private func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(self.width)
        let cellCount = floor(CGFloat(self.view.frame.size.width) / estimatedWidth)
        
        let marginSize = CGFloat(margin * 2)
        
        let width = (self.view.frame.size.width - CGFloat(margin) * (cellCount - 1) - marginSize) / cellCount
        
        return width
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailProductVC()
        detail.product = favorites[indexPath.row]
        self.present(detail, animated: true, completion: nil)
    }
}
