//
//  ListVC.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 24/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialComponents.MaterialBottomSheet
import RealmSwift

class ListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    
    private var data = [Product.Data]()
    private var showedProduct = [Product.Data]()
    private let dispose = DisposeBag()
    private let viewModel = ListViewModel()
    
    private let bottomSheetVC = FilterBottomSheetVC()
    
    private let productRealm = ProductRealmDatabase()
    
    private var width = 200.0
    private var height = 400.0
    private var margin = 16.0

    override func viewDidLoad() {
        super.viewDidLoad()

        toolbar.barTintColor = UIColor.white
        setupCollectionView()
        
        observeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.requestData()
    }
    
    private func observeData(){
        
        self.viewModel.product
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { product in
                if let productData = product.data {
                    self.saveData(data: productData)
                    self.showAllData()
                }
            }, onError: { error in
                
            })
            .disposed(by: self.dispose)
        
    }
    
    func showAllData(){
        self.showedProduct.removeAll()
        self.showedProduct = self.data
        self.collectionView.reloadData()
    }
    
    func showAvailableData(){
        self.showedProduct.removeAll()
        for product in self.data {
            if let isAvailable = product.isAvailable {
                if isAvailable {
                    self.showedProduct.append(product)
                }
            }
        }
        self.collectionView.reloadData()
    }
    
    private func saveData(data: [Product.Data]){
        self.data = data
        let db = self.productRealm.getProduct()
        for productDB in db {
            for product in self.data {
                if(productDB.name == product.name){
                    product.loved = true
                    break
                }
            }
        }
    }
    
    private func setupCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ProductViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductViewCell")
    }

    @IBAction func filterClicked(_ sender: UIButton) {
        bottomSheetVC.listVC = self
        let bottomSheet = MDCBottomSheetController(contentViewController: bottomSheetVC)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 4)
        self.present(bottomSheet, animated: true, completion: nil)
        
    }
    
    // MARK: COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.showedProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductViewCell", for: indexPath) as! ProductViewCell
        cell.setData(data: self.showedProduct[indexPath.row])
        cell.actionBlock = {
            if self.showedProduct[indexPath.row].loved {
                if let productName = self.showedProduct[indexPath.row].name {
                    self.productRealm.deleteRealmObject(name: productName)
                    self.showedProduct[indexPath.row].loved = false
                }
            } else {
                self.productRealm.saveRealmObject(product: self.showedProduct[indexPath.row])
                self.showedProduct[indexPath.row].loved = true
            }
            self.collectionView.reloadItems(at: [indexPath])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = calculateWidth()
        let height = CGFloat(self.height)
        return CGSize(width: width, height: height)
    }
    
    private func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(self.width)
        let cellCount = floor(CGFloat(self.view.frame.size.width) / estimatedWidth)
        
        let marginSize = CGFloat(margin * 2)
        
        let width = (self.view.frame.size.width - CGFloat(margin) * (cellCount - 1) - marginSize) / cellCount
        
        return width
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailProductVC()
        detail.product = showedProduct[indexPath.row]
        self.present(detail, animated: true, completion: nil)
    }

}
