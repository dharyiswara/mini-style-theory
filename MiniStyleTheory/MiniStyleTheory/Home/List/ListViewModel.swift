//
//  ListViewModel.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 24/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire
import ObjectMapper

class ListViewModel{
    
    let product = PublishSubject<Product>()
    let loading = PublishSubject<Bool>()
    
    private let compositeDisposable = CompositeDisposable()
    
    func requestData(page:Int = 1, length:Int = 20){
        self.loading.onNext(true)
        let url = "https://8sp0h28y7j.execute-api.us-east-1.amazonaws.com/dev/styles?page_number=\(page)&per_page=\(length)"
        Alamofire.request(url).responseJSON{ response in
            switch(response.result){
            case .success(_):
                if let data = response.result.value{
                    if let productResponse = Mapper<Product>().map(JSONObject: data){
                        self.product.onNext(productResponse)
                    }
                }
                break
            case .failure(_):
                
                break
            }
        }
    }
    
}
