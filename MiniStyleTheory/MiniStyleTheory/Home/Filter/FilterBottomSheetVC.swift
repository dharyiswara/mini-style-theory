//
//  FilterBottomSheetVC.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 25/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit

class FilterBottomSheetVC: UIViewController {

    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnAvailable: UIButton!
    
    var listVC : ListVC? = nil
    
    var partialView: CGFloat {
        return UIScreen.main.bounds.height - (btnAll.frame.maxY + btnAvailable.frame.maxY + UIApplication.shared.statusBarFrame.height)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        roundedView()
    }

    @IBAction func allClicked(_ sender: UIButton) {
        if let parentVC = listVC {
            parentVC.showAllData()
        }
        close()
    }
    
    @IBAction func availableOnlyClicked(_ sender: UIButton) {
        if let parentVC = listVC {
            parentVC.showAvailableData()
        }
        close()
    }
    
    private func close(){
        dismiss(animated: true, completion: nil)
    }
    
    private func roundedView(){
        let btnRadius = 10
        let viewRadius = 5
        let borderWidth = 1
        
        //View
        view.layer.cornerRadius = CGFloat(viewRadius)
        view.clipsToBounds = true
        
        //Button
        btnAll.layer.cornerRadius = CGFloat(btnRadius)
        
        btnAvailable.layer.cornerRadius = CGFloat(btnRadius)
        btnAvailable.layer.borderWidth = CGFloat(borderWidth)
        btnAvailable.layer.borderColor = UIColor.black.cgColor
    }

}
