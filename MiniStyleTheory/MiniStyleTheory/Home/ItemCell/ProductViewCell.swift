//
//  ProductViewCell.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 24/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit
import SDWebImage

class ProductViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ivProduct: UIImageView!
    @IBOutlet weak var labelAvailable: UILabel!
    @IBOutlet weak var ivLoved: UIButton!
    
    var actionBlock:(()-> Void)? = nil
    
    func setData(data: Product.Data){
        let urlImage = data.images![0].replacingOccurrences(of: ".webp", with: "")
        ivProduct.sd_setImage(with: URL(string: urlImage))
        setLoved(loved: data.loved)
        labelAvailable.isHidden = data.isAvailable!
    }
    
    @IBAction func clickFav(_ sender: UIButton) {
        actionBlock?()
    }
    func setLoved(loved:Bool){
        if(loved){
            ivLoved.setImage(UIImage(named: "ic_loved"), for: .normal)
        }else{
            ivLoved.setImage(UIImage(named: "ic_not_loved"), for: .normal)
        }
    }
    
}
