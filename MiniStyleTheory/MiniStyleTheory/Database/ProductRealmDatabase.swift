//
//  ProductRealmDatabase.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 25/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import Foundation
import RealmSwift

class ProductRealmDatabase {
    
    private let realm = try! Realm()
    
    func saveRealmObject(product:Product.Data) {
        let productRealm = ProductRealm()
        if let name = product.name {
            productRealm.name = name
        }
        if let isAvailable = product.isAvailable {
            productRealm.isAvailable = isAvailable
        }
        if let desc = product.desc {
            productRealm.desc = desc
        }
        
        if let images = product.images {
            for image in images {
                productRealm.images.append(image)
            }
        }
        
        try! realm.write {
            realm.add(productRealm)
        }
    }
    
    func deleteRealmObject(name: String){
        let product = realm.objects(ProductRealm.self).filter("name = '\(name)'")
        try! realm.write {
            realm.delete(product)
        }
    }
    
    func getProduct() -> [Product.Data] {
        var product = [Product.Data]()
        for data in realm.objects(ProductRealm.self) {
            let productData = Product.Data()
            productData.name = data.name
            productData.isAvailable = data.isAvailable
            for image in data.images {
                productData.images?.append(image)
            }
            productData.desc = data.desc
            product.append(productData)
        }
        return product
    }
    
}
