//
//  DetailProductVC.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 24/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit
import ImageSlideshow

class DetailProductVC: UIViewController {

    @IBOutlet weak var ivBack: UIButton!
    @IBOutlet weak var slideshowProduct: ImageSlideshow!
    @IBOutlet weak var labelProduct: UILabel!
    @IBOutlet weak var tvDesc: UITextView!
    @IBOutlet weak var btnWishlist: UIButton!
    
    var product : Product.Data? = nil
    
    private var imageSource = [SDWebImageSource]()
    
    private let productRealm = ProductRealmDatabase()
    
    private let THREE_SECONDS = 3.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }

    @IBAction func backClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func wishlistClicked(_ sender: UIButton) {
        if let data = product {
            if data.loved {
                data.loved = false
                if let productName = data.name {
                    productRealm.deleteRealmObject(name: productName)
                }
                setButtonAddWishlist()
            } else {
                data.loved = true
                productRealm.saveRealmObject(product: data)
                setButtonRemoveWishlist()
            }
        }
    }
    
    private func setupImageSlide(data:Product.Data){
        slideshowProduct.slideshowInterval = THREE_SECONDS
        slideshowProduct.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshowProduct.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        slideshowProduct.pageIndicator = pageControl
        
        if let images = data.images {
            for image in images {
                let imageUrl = image.replacingOccurrences(of: ".webp", with: "")
                imageSource.append(SDWebImageSource(urlString: imageUrl)!)
            }
        }
        
        slideshowProduct.setImageInputs(imageSource)
    }
    
    private func initView(){
        let btnRadius = 5
        
        if let data = product {
            setupImageSlide(data:data)
            labelProduct.text = data.name
            tvDesc.text = data.desc
            
            if data.loved {
                setButtonRemoveWishlist()
            } else {
                setButtonAddWishlist()
            }
        }
        
        btnWishlist.layer.cornerRadius = CGFloat(btnRadius)
        
    }
    
    private func setButtonAddWishlist(){
        let borderWidth = 0
        btnWishlist.setTitle("ADD TO WISHLIST", for: .normal)
        btnWishlist.backgroundColor = UIColor.init(red: 18/255, green: 140.0/255, blue: 28.0/255, alpha: 1)
        btnWishlist.setTitleColor(UIColor.white, for: .normal)
        btnWishlist.layer.borderWidth = CGFloat(borderWidth)
    }
    
    private func setButtonRemoveWishlist(){
        let borderWidth = 1
        btnWishlist.setTitle("REMOVE FROM WISHLIST", for: .normal)
        btnWishlist.backgroundColor = UIColor.white
        btnWishlist.setTitleColor(UIColor.black, for: .normal)
        btnWishlist.layer.borderWidth = CGFloat(borderWidth)
    }

}
