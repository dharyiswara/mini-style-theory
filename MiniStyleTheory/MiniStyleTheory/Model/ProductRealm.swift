//
//  ProductRealm.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 25/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import Foundation
import RealmSwift

class ProductRealm: Object {

    @objc dynamic var name = ""
    @objc dynamic var isAvailable = false
    var images = List<String>()
    @objc dynamic var desc = ""
    
}
