//
//  Product.swift
//  MiniStyleTheory
//
//  Created by Dary Hilmy Iswara on 24/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import Foundation
import ObjectMapper

class Product : Mappable {
    
    var data : [Data]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
    class Data : Mappable {
        var name : String?
        var isAvailable : Bool?
        var images : [String]?
        var desc : String?
        var loved : Bool = false
        
        init() {
            name = ""
            isAvailable = false
            images = [String]()
            desc = ""
        }
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            name <- map["name"]
            isAvailable <- map["is_available"]
            images <- map["images"]
            desc <- map["description"]
        }
        
    }
}
